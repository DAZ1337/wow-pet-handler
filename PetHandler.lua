-- Constant table for available call pet spells. This is used to count the maximum
-- number of pet slots the player currently has access to. If the player only has
-- one slot, then the addon will not remind the user to deposit a pet.
local CALL_PET_SPELL_IDS = {
    0883,
    83242,
    83243,
    83244,
    83245,
};

-- Create the addon frame and register events to that frame. Frame script will contain
-- the logic executed for reminding the user to deposit a pet.
f = CreateFrame("Frame")
f:RegisterEvent("ZONE_CHANGED")
f:RegisterEvent("PET_STABLE_SHOW")
f:RegisterEvent("PLAYER_ENTERING_WORLD")
f:RegisterEvent("PET_UI_UPDATE")
f:SetScript("OnEvent", function(self, event, ...)
    
    -- Determine the amount of slots available vs. used at the time of the call.
    local maxslots, usedslots = 1, 0
    while IsSpellKnown(CALL_PET_SPELL_IDS[maxslots + 1]) do maxslots = maxslots + 1 end
    for i=1,maxslots,1 do 
        local _, petname = GetCallPetSpellInfo(CALL_PET_SPELL_IDS[i])
        if petname ~= '' then usedslots = usedslots + 1 end
    end
    
    -- Remind the user if the player doesn't have available slots and has the ability to
    -- obtain more than one pet.
    if maxslots > 1 and maxslots == usedslots then
        DEFAULT_CHAT_FRAME:AddMessage('Pet Handler: You are out of available slots for taming!', 1.0, 0, 0)
    end
end)