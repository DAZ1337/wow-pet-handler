# Pet Handler
The point of this project was to gain experience working with the World of Warcraft UI API, authored in Lua. This addon uses event triggers to notify the player when they enter a new zone to deposit a pet in the stable if their pet slots are full. This avoids the problem of not being able to tame a pet due to full slots when exploring the world. Addons in World of Warcraft can be much more complicated than this example; however, this project can be used as a template for creating other addons.

## Install
To install the addon, download this repository and extract into "...\World of Warcraft\Interface\AddOns\" (wherever you have the game installed). Exit and start a new game client. In the addon menu, you should now see that "Pet Handler" is enabled and loaded.
